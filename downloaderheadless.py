#!/usr/bin/env python3
"""
Browser-automated downloader, headless version

Copyright (C) 2018-2021 cosimone64

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

You should have received a copy of the GNU AGPLv3 with this software,
if not, please visit <https://www.gnu.org/licenses/>
"""

import os
import pickle
import sys
import threading
from threading import Thread

import requests
from bs4 import BeautifulSoup

# Constant parameters
HEADERS = {'User-agent': 'Mozilla/5.0'}
CHUNKUNIT = 2**10
CHUNKSIZE = 1 * CHUNKUNIT
MAX_THREADNUM = 50
TERMINATION_CONTENT = b'Nothing found'
DBFILE = 'dbfile'
PAGE_FETCHED_ERROR_STATUS = -1
PAGE_FETCHED_MORE_PAGES_STATUS = 1
PAGE_FETCHED_END_STATUS = 0


def get_video_urls(main_url, get, index):
    """Return a list containing valid video URLs. in the specified page."""
    index_page_url = main_url + '/' + str(index)
    index_page = get(index_page_url, headers=HEADERS)
    if not index_page.ok:
        return [], PAGE_FETCHED_ERROR_STATUS
    if TERMINATION_CONTENT in index_page.content:
        return [], PAGE_FETCHED_END_STATUS

    index_soup = BeautifulSoup(index_page.content, 'lxml')
    tiles = index_soup.find_all('div', {'class': 'tile'})
    thumbnails = [tile.find('a', {'class': 'thumb'}) for tile in tiles]
    return [
        thumb.attrs['href'] for thumb in thumbnails
        if 'out.php' not in thumb.attrs['href']
    ], PAGE_FETCHED_MORE_PAGES_STATUS


def get_page_video_dict(main_url, get):
    """Obtain a dictionary mapping each page with a list of video URLs it
    contains.  The get parameter is a function (or possibly, a bound method)
    used to retrieve pages.

    """
    local_dict = {}
    last_page_reached = False
    pagecounter = 1

    print('Building video URL database...')

    while not last_page_reached:
        print('Fecthing from index page ' + str(pagecounter))
        url_list, status = get_video_urls(main_url, get, pagecounter)
        if status == PAGE_FETCHED_ERROR_STATUS:
            print('Error fetching page ' + main_url + '/' + str(pagecounter))
        elif status == PAGE_FETCHED_END_STATUS:
            print('Reached last page')
            last_page_reached = True
        else:
            local_dict[pagecounter] = url_list
        pagecounter += 1
    return local_dict


def print_video_index(page_index, video_urls):
    """Given page_index, print out a numbered list showing its valid video
    URLs.

    """
    print('Videos for page ' + str(page_index))
    indices = range(1, len(video_urls) + 1)
    for index, url in zip(indices, video_urls):
        print(index + '\t' + url)


def get_filename_to_download(main_url, video_page_url):
    """Return the name of the file to download."""
    domain = main_url.split('//')[-1].split('/')[0]
    return (video_page_url.split(domain + '/')[-1].replace('/', '_').replace(
        '.html', '.mp4').replace(':', "_"))


def scrape_video_page(video_page_url, main_url, get):
    """Scrapes video page to get the actual video link, then downloads it and
    saves it to a file.  Pages are fetches using the get function passed as
    argument. It must accept the same parameters as the get method from
    requests.

    """
    filename = get_filename_to_download(main_url, video_page_url)
    if os.path.exists(filename):
        print('File ' + filename + ' already exists, skipping...')
        return

    print('Feching from ' + video_page_url)
    video_page = get(video_page_url, headers=HEADERS)
    video_soup = BeautifulSoup(video_page.content, 'lxml')

    video_element = video_soup.find('video')
    if not video_element:
        print('Page ' + video_page_url +
              ' doesn\'t seem to contain a video, skipping...')
        return

    video_url = video_element.find('source').attrs['src']
    if not video_url:
        print('Video url seems empty, skipping...')
        return

    partial_filename = filename + '.part'
    if os.path.exists(partial_filename):
        os.remove(partial_filename)

    remote_video_file = get(video_url, headers=HEADERS, stream=True)
    print('Downloading ' + filename + ' ...')
    with open(partial_filename, 'wb') as output_file:
        for chunk in remote_video_file.iter_content(chunk_size=CHUNKSIZE):
            if chunk:
                output_file.write(chunk)

    os.rename(partial_filename, filename)
    print('Downloading ' + filename + ' complete!')


def main(main_url, proxy=None):
    """Main function, also called if this program is executed as a top level
    script.

    """
    with requests.sessions.Session() as session:
        session.proxies = ({
            'http': proxy,
            'https': proxy
        } if proxy is not None else None)
        get = session.get
        if os.path.exists(DBFILE):
            print('Reusing existing video URL database')
            with open(DBFILE, 'rb') as dbfile:
                page_video_dict = pickle.load(dbfile)
        else:
            page_video_dict = get_page_video_dict(main_url, get)
            with open(DBFILE, 'wb') as dbfile:
                pickle.dump(page_video_dict, dbfile)

        for video_url_list in page_video_dict.values():
            for url in video_url_list:
                while threading.active_count() > MAX_THREADNUM:
                    pass  # A condition variable would be better here...
                thread = Thread(target=scrape_video_page,
                                args=(url, main_url, get))
                thread.start()
    print('Waiting for completion of remaining threads...')


if __name__ == '__main__':
    argv = sys.argv
    if len(argv) < 2 or len(argv) > 3:
        sys.stderr.write('Use as ' + argv[0] + ' <url> [<proxy>]\n')
    elif len(argv) == 2:
        main(argv[1])
    else:
        main(argv[1], argv[2])
